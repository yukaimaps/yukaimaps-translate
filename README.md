Yukaidi translate
=================

Python tools to manage json translations with gettext tools
into Yukaimaps projects.

- update POT catalog and PO files from an existing template (en.json)
- build minified json for each supported language after translations


Install
-------

Make a python virtualenv and install dependencies

```
python3 -m venv venv
source venv/bin/activate
pip install -r requirements.txt
```


Translate
---------

### Get news from template json file

Update existing `po/*.po` files from an existing schema build

```
source venv/bin/activate
python translator.py update -t ../path/to/en.json -d ../path/to/i18n/
```

### Translate

Edit each `../path/to/i18n/*.po` file which your preferred tools (as Poedit)


### Rebuild translations as json

Make `json/*.json` files from `.po` files

```
source venv/bin/activate
python translator.py build -t ../path/to/en.json -d ../path/to/i18n/
```

### Preconfigured scripts


- [update_yukaidi_tagging_schema.sh](./update_yukaidi_tagging_schema.sh)
- [build_yukaidi_tagging_schema.sh](./build_yukaidi_tagging_schema.sh)

- [update_yukaidi.sh](./update_yukaidi.sh)
- [build_yukaidi.sh](./build_yukaidi.sh)
