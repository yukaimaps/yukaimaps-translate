#!/bin/bash

python translator.py build \
    -d ../yukaidi/data/i18n \
    -t ../yukaidi/data/en.json
